# setup.py
import setuptools


# load README.md
with open("README.md", "r") as f:
    load_description = f.read()


setuptools.setup(
    name="testing-for-pip",
    version="0.0.1",
    author="tkok",
    author_email="okawabb@gmail.com",
    description="A small example package",
    long_description=load_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tkok/testing-for-pip",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'requests',
    ]
)
